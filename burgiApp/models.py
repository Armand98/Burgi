from django.db import models



class Meal(models.Model):
    nazwa = models.CharField(max_length=200)
    cena = models.DecimalField (default=0.0, max_digits=5, decimal_places=2)
    ilosc = models.IntegerField(default=1)

    def __str__(self):
        customStr = self.nazwa + " - " + str(self.cena) + " zł"
        return customStr



class Drink(models.Model):
    nazwa = models.CharField(max_length=200)
    cena = models.DecimalField (default=0.0, max_digits=5, decimal_places=2)
    ilosc = models.IntegerField(default=1)

    def __str__(self):
        customStr = self.nazwa + " - " + str(self.cena) + " zł"
        return customStr



class Ingredient(models.Model):
    nazwa = models.CharField(max_length=200)
    cena_hurtowa = models.DecimalField (default=0.0, max_digits=5, decimal_places=2)
    ilosc = models.IntegerField(default=1)

    UNITS = (
        ('g','g'),
        ('dag','dag'),
        ('kg','kg'),
        ('sztuk', 'sztuk'),
    )

    jednostka = models.CharField(max_length=200, choices=UNITS, default="g")

    AVAILABILITY = (
        ('Duzo', 'Duża ilość'),
        ('Srednio', 'Średnia ilość'),
        ('Malo', 'Bliskie wyczerpaniu'),
        ('Brak', 'Brak'),
    )

    stan_w_magazynie = models.CharField(max_length=200, choices=AVAILABILITY, default="Brak")

    def __str__(self):
        return self.nazwa



class Order(models.Model):
    ORDERTYPE = (
        ('Na miejscu', 'Na miejscu'),
        ('Na wynos', 'Na wynos'),
    )

    TABLEID = (
        ('0', 'Na wynos'),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
    )

    STATUS = (
        ('Oczekujace', 'Oczekujace'),
        ('Przygotowywane', 'Przygotowywane'),
        ('W trasie', 'W trasie'),
        ('Dostarczone', 'Dostarczone'),
    )

    typ_zamowienia = models.CharField(max_length=200, choices=ORDERTYPE, default="Na miejscu")
    numer_stolika = models.CharField(max_length=200, choices=TABLEID, default="1")
    dania = models.ManyToManyField(Meal)
    napoje = models.ManyToManyField(Drink)
    data_zamowienia = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    notatka = models.CharField(max_length=1000, default="Brak")
    status = models.CharField(max_length=200, choices=STATUS, default="Oczekujace")
    rabat = models.IntegerField(default=0)
    suma = models.DecimalField (default=0.0, max_digits=5, decimal_places=2)

    def __str__(self):
        formatedDate = self.data_zamowienia.strftime("%Y-%m-%d %H:%M:%S")
        data = str(formatedDate) + " / " + self.typ_zamowienia + " / Stolik: " + self.numer_stolika
        return data
    

