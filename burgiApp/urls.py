from django.urls import path

from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('menu/', views.menu, name='menu'),
    path('orders/', views.ordersPage, name='orders'),
    path('supplies/', views.suppliesPage, name='supplies'),

    path('update_order/<str:pk>/', views.updateOrder, name="update_order"),
    path('delete_order/<str:pk>/', views.deleteOrder, name="delete_order"),

    path('update_meal/<str:pk>/', views.updateMeal, name="update_meal"),
    path('delete_meal/<str:pk>/', views.deleteMeal, name="delete_meal"),

    path('update_menu_drink/<str:pk>/', views.updateMenuDrink, name="update_menu_drink"),
    path('update_supply_drink/<str:pk>/', views.updateSupplyDrink, name="update_supply_drink"),
    path('delete_drink/<str:pk>/', views.deleteDrink, name="delete_drink"),

    path('update_ingredient/<str:pk>/', views.updateIngredient, name="update_ingredient"),
    path('delete_ingredient/<str:pk>/', views.deleteIngredient, name="delete_ingredient"),
]

'''
1 - Submit email form                         //PasswordResetView.as_view()
2 - Email sent success message                //PasswordResetDoneView.as_view()
3 - Link to password Rest form in email       //PasswordResetConfirmView.as_view()
4 - Password successfully changed message     //PasswordResetCompleteView.as_view()
'''