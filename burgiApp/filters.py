
import django_filters
from django_filters import DateFilter, CharFilter
from django import forms
from .models import *

class OrderFilter(django_filters.FilterSet):
	class Meta:
		model = Order
		fields = ['typ_zamowienia', 'dania', 'napoje', 'rabat']

	Data_zamowienia_od = DateFilter(field_name="data_zamowienia", lookup_expr='gte')
	Data_zamowienia_do = DateFilter(field_name="data_zamowienia", lookup_expr='lte')

	dania = forms.ModelMultipleChoiceField(
		widget=forms.CheckboxSelectMultiple(),
		queryset=Meal.objects.all())
