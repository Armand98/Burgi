from django.apps import AppConfig


class BurgiappConfig(AppConfig):
    name = 'burgiApp'

    def ready(self):
        import burgiApp.signals
