from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User
from .models import *

class OrderForm(ModelForm):
	class Meta:
		model = Order
		fields = ['typ_zamowienia', 'numer_stolika', 'dania', 'napoje', 'notatka', 'status', 'rabat', 'suma']

	ORDERTYPE = (
        ('Na miejscu', 'Na miejscu'),
        ('Na wynos', 'Na wynos'),
    )

	typ_zamowienia = forms.CharField(max_length=200,
		widget=forms.Select(choices=ORDERTYPE,
			attrs={"onchange":"orderTypeChanged(this.value)"}))

	dania = forms.ModelMultipleChoiceField(
		widget=forms.CheckboxSelectMultiple(
			attrs={"class":"productItem", "onchange":"addProduct(this)"}),
		queryset=Meal.objects.all())

	napoje = forms.ModelMultipleChoiceField(
		widget=forms.CheckboxSelectMultiple(
			attrs={"class":"productItem", "onchange":"addProduct(this)"}),
		queryset=Drink.objects.all())

	rabat = forms.IntegerField(min_value=0, max_value=100, initial=0,
			widget=forms.NumberInput(attrs={"onchange":"updatePrice()"}))



class MenuMealForm(ModelForm):
	class Meta:
		model = Meal
		fields = ['nazwa', 'cena']



class MenuDrinkForm(ModelForm):
	class Meta:
		model = Drink
		fields = ['nazwa', 'cena']



class SupplyDrinkForm(ModelForm):
	class Meta:
		model = Drink
		fields = ['nazwa', 'cena', 'ilosc']



class SupplyIngredientForm(ModelForm):
	class Meta:
		model = Ingredient
		fields = ['nazwa', 'cena_hurtowa', 'ilosc', 'jednostka']



class CreateUserForm(UserCreationForm):
	class Meta:
		model = User
		fields = ['username', 'email', 'password1', 'password2']
