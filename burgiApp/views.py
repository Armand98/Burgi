from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group

from .models import *
from .forms import *
from .filters import *
from .decorators import unauthenticated_user, allowed_users, admin_only


def home(request):
	orders = Order.objects.all()
	total_orders = orders.count()
	delivered = orders.filter(status='Dostarczone').count()
	pending = orders.filter(status='Oczekujace').count()

	context = {'total_orders':total_orders,
	'delivered':delivered, 'pending':pending}

	return render(request, 'burgiApp/dashboard.html', context)



def menu(request):
	meals = Meal.objects.all()
	drinks = Drink.objects.all()
	mealForm = MenuMealForm()
	drinkForm = MenuDrinkForm()

	if request.method == 'POST':
		if 'SubmitMeal' in request.POST:
			mealForm = MenuMealForm(request.POST)
			if mealForm.is_valid():
				mealForm.save()
				return redirect('menu')
		elif 'SubmitDrink' in request.POST:
			drinkForm = MenuDrinkForm(request.POST)
			if drinkForm.is_valid():
				drinkForm.save()
				return redirect('menu')

	context = {'meals':meals, 'drinks':drinks,
			'mealForm':mealForm, 'drinkForm':drinkForm}
	return render(request, 'burgiApp/menu.html', context)



def suppliesPage(request):
	ingredients = Ingredient.objects.all()
	drinks = Drink.objects.all()
	ingredientForm = SupplyIngredientForm()
	drinkForm = SupplyDrinkForm()

	if request.method == 'POST':
		if 'SubmitIngredient' in request.POST:
			ingredientForm = SupplyIngredientForm(request.POST)
			if ingredientForm.is_valid():
				ingredientForm.save()
				return redirect('supplies')
		elif 'SubmitDrink' in request.POST:
			drinkForm = SupplyDrinkForm(request.POST)
			if drinkForm.is_valid():
				drinkForm.save()
				return redirect('supplies')

	context = {'ingredients':ingredients, 'drinks':drinks,
	'ingredientForm':ingredientForm, 'drinkForm':drinkForm }
	return render(request, 'burgiApp/supplies.html', context)



def updateIngredient(request, pk):
	ingredient = Ingredient.objects.get(id=pk)
	ingredientForm = SupplyIngredientForm(instance=ingredient)

	if request.method == 'POST':
		ingredientForm = SupplyIngredientForm(request.POST, instance=ingredient)
		if ingredientForm.is_valid():
			ingredientForm.save()
			return redirect('supplies')

	context = {'ingredientForm':ingredientForm}
	return render(request, 'burgiApp/supplies.html', context)



def deleteIngredient(request, pk):
	ingredient = Ingredient.objects.get(id=pk)

	if request.method == "POST":
		ingredient.delete()
		return redirect('supplies')

	context = {'item':ingredient}
	return render(request, 'burgiApp/delete_ingredient.html', context)



def updateMeal(request, pk):
	meal = Meal.objects.get(id=pk)
	mealForm = MenuMealForm(instance=meal)
	if request.method == 'POST':
		mealForm = MenuMealForm(request.POST, instance=meal)
		if mealForm.is_valid():
			mealForm.save()
			return redirect('menu')

	context = {'mealForm':mealForm}
	return render(request, 'burgiApp/menu.html', context)



def deleteMeal(request, pk):
	meal = Meal.objects.get(id=pk)
	if request.method == "POST":
		meal.delete()
		return redirect('menu')

	context = {'item':meal}
	return render(request, 'burgiApp/delete_meal.html', context)



def updateMenuDrink(request, pk):
	drink = Drink.objects.get(id=pk)
	drinkForm = MenuDrinkForm(instance=drink)
	if request.method == 'POST':
		drinkForm = MenuDrinkForm(request.POST, instance=drink)
		if drinkForm.is_valid():
			drinkForm.save()
			return redirect('menu')

	context = {'drinkForm':drinkForm}
	return render(request, 'burgiApp/menu.html', context)



def updateSupplyDrink(request, pk):
	drink = Drink.objects.get(id=pk)
	drinkForm = SupplyDrinkForm(instance=drink)
	if request.method == 'POST':
		drinkForm = SupplyDrinkForm(request.POST, instance=drink)
		if drinkForm.is_valid():
			drinkForm.save()
			return redirect('supplies')

	context = {'drinkForm':drinkForm}
	return render(request, 'burgiApp/menu.html', context)



def deleteDrink(request, pk):
	drink = Drink.objects.get(id=pk)
	if request.method == "POST":
		drink.delete()
		return redirect('menu')

	context = {'item':drink}
	return render(request, 'burgiApp/delete_drink.html', context)



def ordersPage(request):
	orders = Order.objects.all()
	total_orders = orders.count()
	delivered = orders.filter(status='Dostarczone').count()
	pending = orders.filter(status='Oczekujace').count()
	form = OrderForm()

	if request.method == 'POST':
		form = OrderForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('orders')

	filter = OrderFilter(request.GET, queryset=orders)
	orders = filter.qs

	context = {'orders':orders, 'form':form, 'total_orders':total_orders,
				'delivered':delivered, 'pending':pending, 'filter':filter}

	return render(request, 'burgiApp/orders.html', context)



def updateOrder(request, pk):
	order = Order.objects.get(id=pk)
	form = OrderForm(instance=order)
	if request.method == 'POST':
		form = OrderForm(request.POST, instance=order)
		if form.is_valid():
			form.save()
			return redirect('orders')

	context = {'form':form}
	return render(request, 'burgiApp/orders.html', context)



def deleteOrder(request, pk):
	order = Order.objects.get(id=pk)
	if request.method == "POST":
		order.delete()
		return redirect('orders')

	context = {'item':order}
	return render(request, 'burgiApp/delete_order.html', context)
